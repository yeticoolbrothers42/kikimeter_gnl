/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pibenoit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 18:46:37 by pibenoit          #+#    #+#             */
/*   Updated: 2016/01/20 17:35:24 by agadhgad         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include "get_next_line.h"
#include <unistd.h>

void	ft_read(int fd)
{
	char *line;
	int r;

	while ((r = get_next_line(fd, &line)) > 0)
	{
		printf("%d - |%s|\n", r, line);
		free(line);
		line = NULL;
	}
		printf("LAST - %d|\n", r);
}

int main(int ac, char **av)
{
	int fd;

	if (ac == 2)
	{
		fd = open(av[1], O_RDONLY);
		ft_read(fd);
	}
	else
		ft_read(0);
	return (0);
}
